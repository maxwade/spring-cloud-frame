package com.example.Controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/")
public class UserController {

    // 登录接口
    @RequestMapping("doLogin")
    public SaResult doLogin() {
        // 第1步，先登录上 
        StpUtil.login(10001);
        // 第2步，获取 Token  相关参数 
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        // 第3步，返回给前端 
        return SaResult.data(tokenInfo);
    }

    @RequestMapping("isLogin")
    public SaResult isLogin() {
        return SaResult.ok("当前会话是否登录：" + StpUtil.isLogin());
    }

    @RequestMapping("add")
    public SaResult add() {
        return SaResult.ok("用户注册");
    }

    @RequestMapping("delete")
    public SaResult delete() {
        return SaResult.ok("用户删除");
    }

}    